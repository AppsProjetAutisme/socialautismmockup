#!/usr/bin/env bash

curl -O https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.gitpush.sh && chmod +x tools.gitpush.sh;

# This directory path
DIR="$(cd "$(dirname "$0")" && pwd -P)"
# Full path of this script
THIS="${DIR}/$(basename "$0")"

git fetch --all
git pull origin master
git add --all
git commit --author="$(whoami) <$(whoami)@$HOSTNAME>" -am "$(whoami)@$HOSTNAME $(TZ="Europe/Paris" date +'%Y-%m-%d %H:%M %Z %A')"
git push origin master
git pull origin master

curl -O https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.log.git.sh
chmod +x tools.log.git.sh
./tools.log.git.sh

# chargement librairie insert crontab
timestamp=`date +%Y%m%d%H%M%S`
curl -s https://gitlab.com/CoreDockWorker/coredockworker.tools.public/raw/master/tools.function.insertcrontab.sh -o /tmp/.myscript.${timestamp}.tmp
source /tmp/.myscript.${timestamp}.tmp
rm -f /tmp/.myscript.${timestamp}.tmp

# install and update crontab atv_cron
insertcrontab "${THIS}"
